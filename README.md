# test-coverage-ci

Simple demo of GitLab CI/CD for a pipeline job that block unittest coverage below a threshold.

We purposely fail the pipeline at a coverage of 40%.
